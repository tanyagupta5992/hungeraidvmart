package com.hungeraid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HungeraidWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(HungeraidWebApplication.class, args);
	}

}
