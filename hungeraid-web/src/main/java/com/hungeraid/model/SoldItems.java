package com.hungeraid.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "sold_item")
public class SoldItems {
	
	@Transient
	public static final String SEQUENCE_NAME = "sold_items_sequence";
	
	@Id
	private String _id;
	private Double totalPrice;
	private Double discountedPrice;
	private String modeOfPayment;
	private Integer numberOfItems;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;

}
