package com.hungeraid.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection = "database_sequence")
@Data @NoArgsConstructor @AllArgsConstructor
public class DatabaseSequence {

	@Id
	private String _id;
	
	private String sequence;
	
}
