package com.hungeraid.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "product_category")
public class ProductCategory {
	
	@Transient
	public static final String SEQUENCE_NAME = "product_category_sequence";
	
	@Id
	private String _id;
	private String categoryName;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;

}
