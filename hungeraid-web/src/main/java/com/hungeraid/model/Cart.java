package com.hungeraid.model;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "cart")
public class Cart {
	
	@Transient
	public static final String SEQUENCE_NAME = "cart_sequence";
	
	@Id
	private String _id;
	private String userId;
	private List<String> productIdList;
	private Integer numberOfItems;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;

}
