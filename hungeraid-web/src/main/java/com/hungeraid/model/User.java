package com.hungeraid.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "user")
public class User {
	
	@Transient
	public static final String SEQUENCE_NAME = "users_sequence";
	
	@Id
	private String _id;
	private String firstName;
	private String middleName;
	private String lastName;
	private String emailId;
	private String contactNumber;
	private String address;
	private Integer pincode;
	private String userType;
	private Boolean activeInd;
	private String password;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;

}
