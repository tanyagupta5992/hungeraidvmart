package com.hungeraid.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "invoice")
public class Invoices {
	
	@Transient
	public static final String SEQUENCE_NAME = "invoice_sequence";
	
	@Id
	private String _id;
	private String userId;
	private Double totalPrice;
	private String shopId;
	private String modeOfPayment;
	private Integer numberOfItems;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
}
