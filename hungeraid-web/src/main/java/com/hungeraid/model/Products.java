package com.hungeraid.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "products")
public class Products {

	@Transient
	public static final String SEQUENCE_NAME = "products_sequence";
	
	@Id
	private String _id;
	private String productName;
	private Double productPrice;
	private Double productDiscountPct;
	private String productCategory;
	private String productBrand;
	private String productDescription;
	private Date productMfgDate;
	private Date productExpDate;
	private String shopId;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	
}
