package com.hungeraid.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Document(collection = "shop")
public class Shop {
	
	@Transient
	public static final String SEQUENCE_NAME = "shop_sequence";
	
	@Id
	private String _id;
	private String shopName;
	private LocationCoordinates locationCoordinates;
	private Boolean activeInd;
	private String contactNumber;
	private String shopOwnerId;
	private String address;
	private String pincode;
	private String gstNumeber;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;

}

@Data @AllArgsConstructor @NoArgsConstructor
class LocationCoordinates{
	private String lat;
	private String lon;
}
