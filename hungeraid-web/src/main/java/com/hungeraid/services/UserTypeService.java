package com.hungeraid.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hungeraid.model.UserType;
import com.hungeraid.repository.UserTypeRepository;

@Service
public class UserTypeService {
	
	@Autowired
	private UserTypeRepository userTypeRepository;
	
	public List<UserType> getAllUserType(){
		List<UserType> userTypeList = userTypeRepository.findAll();
		return userTypeList;
	}

}
