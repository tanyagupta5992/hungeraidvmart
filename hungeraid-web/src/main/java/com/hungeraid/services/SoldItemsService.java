package com.hungeraid.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hungeraid.model.SoldItems;
import com.hungeraid.repository.SoldItemsRepository;

@Service
public class SoldItemsService {
	@Autowired
	private SoldItemsRepository soldItemsRepository;

	public List<SoldItems> getAllSoldItems(){
		List<SoldItems> soldItemList = soldItemsRepository.findAll();
		return soldItemList;
	}
}
