package com.hungeraid.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hungeraid.model.ProductCategory;
import com.hungeraid.repository.ProductCategoryRepository;

@Service
public class ProductCategoryService {
	
	@Autowired
	private ProductCategoryRepository productCategoryRepository;
	
	public List<ProductCategory> getAllProductCategory(){
		List<ProductCategory> productCategoryList = productCategoryRepository.findAll();
		return productCategoryList;
	}

}
