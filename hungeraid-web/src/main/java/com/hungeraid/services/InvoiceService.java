package com.hungeraid.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hungeraid.model.Invoices;
import com.hungeraid.repository.InvoicesRepository;

@Service
public class InvoiceService {

	@Autowired
	private InvoicesRepository invoicesRepository;
	
	public List<Invoices> getAllInvoices(){
		List<Invoices> invoicesList = invoicesRepository.findAll();
		return invoicesList;
	}
}
