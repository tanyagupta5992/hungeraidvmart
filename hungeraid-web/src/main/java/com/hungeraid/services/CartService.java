package com.hungeraid.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hungeraid.model.Cart;
import com.hungeraid.repository.CartRepository;

@Service
public class CartService {

	@Autowired
	private CartRepository cartRepository;
	
	public List<Cart> getAllCarts(){
		List<Cart> cartList = cartRepository.findAll();
		return cartList;
	}
	
}
