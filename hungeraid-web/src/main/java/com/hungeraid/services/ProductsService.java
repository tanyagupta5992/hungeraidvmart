package com.hungeraid.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hungeraid.model.Products;
import com.hungeraid.repository.ProductsRepository;

@Service
public class ProductsService {
	
	@Autowired
	private ProductsRepository productsRepository;
	
	public List<Products> getAllProducts(){
		List<Products> productsList = productsRepository.findAll();
		return productsList;
	}

}
