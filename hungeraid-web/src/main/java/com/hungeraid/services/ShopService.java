package com.hungeraid.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hungeraid.model.Shop;
import com.hungeraid.repository.ShopRepository;

@Service
public class ShopService {
	
	@Autowired
	private ShopRepository shopRepository;
	
	public List<Shop> getAllShops(){
		List<Shop> shopList = shopRepository.findAll();
		return shopList;
	}

}
