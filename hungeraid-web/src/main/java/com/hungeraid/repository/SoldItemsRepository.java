package com.hungeraid.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.hungeraid.model.SoldItems;

@Repository
public interface SoldItemsRepository extends MongoRepository<SoldItems, String>{

}
