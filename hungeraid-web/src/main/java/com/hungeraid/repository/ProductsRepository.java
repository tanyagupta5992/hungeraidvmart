package com.hungeraid.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.hungeraid.model.Products;

@Repository
public interface ProductsRepository extends MongoRepository<Products, String>{

}
