package com.hungeraid.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.hungeraid.model.Invoices;

@Repository
public interface InvoicesRepository extends MongoRepository<Invoices, String>{

}
