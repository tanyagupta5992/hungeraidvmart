package com.hungeraid.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.hungeraid.model.Cart;

@Repository
public interface CartRepository extends MongoRepository<Cart, String>{

}
