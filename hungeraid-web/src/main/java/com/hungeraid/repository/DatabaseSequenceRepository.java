package com.hungeraid.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.hungeraid.model.DatabaseSequence;

@Repository
public interface DatabaseSequenceRepository extends MongoRepository<DatabaseSequence, String>{

}
