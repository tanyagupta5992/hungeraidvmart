package com.hungeraid.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.hungeraid.model.Shop;

@Repository
public interface ShopRepository extends MongoRepository<Shop, String>{

}
