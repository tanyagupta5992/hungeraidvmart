package com.hungeraid.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.hungeraid.model.UserType;

@Repository
public interface UserTypeRepository extends MongoRepository<UserType, String>{

}
