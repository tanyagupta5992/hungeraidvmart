package com.hungeraid.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hungeraid.model.Shop;
import com.hungeraid.services.ShopService;

@RestController
@RequestMapping(path = "/api/shop")
public class ShopController {

	@Autowired
	private ShopService shopService;

	@RequestMapping(path = "/all", method = RequestMethod.GET)
	public List<Shop> getAllShops() {
		return shopService.getAllShops();
	}

}
