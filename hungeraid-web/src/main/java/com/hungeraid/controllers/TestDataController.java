package com.hungeraid.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hungeraid.model.DatabaseSequence;
import com.hungeraid.model.User;
import com.hungeraid.model.UserType;
import com.hungeraid.repository.CartRepository;
import com.hungeraid.repository.DatabaseSequenceRepository;
import com.hungeraid.repository.InvoicesRepository;
import com.hungeraid.repository.ProductCategoryRepository;
import com.hungeraid.repository.ProductsRepository;
import com.hungeraid.repository.ShopRepository;
import com.hungeraid.repository.SoldItemsRepository;
import com.hungeraid.repository.UserRepository;
import com.hungeraid.repository.UserTypeRepository;

@RestController
@RequestMapping(path = "/api/testData")
public class TestDataController {
	
	@Autowired
	private UserTypeRepository userTypeRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private CartRepository cartRepo;
	
	@Autowired
	private ProductsRepository productRepo;
	
	@Autowired
	private ProductCategoryRepository productCatRepo;
	
	@Autowired
	private InvoicesRepository invoRepo;
	
	@Autowired
	private SoldItemsRepository soldItemRepo;
	
	@Autowired
	private ShopRepository shopRepo;
	
	@Autowired
	private DatabaseSequenceRepository dbSeqRepo;
	

	@RequestMapping(path = "insert", method = RequestMethod.GET)
	public String insertTestData() {
		insertData();
		return "Test data inserted";
	}
	
	private void insertData() {
		DatabaseSequence dataSequence = new DatabaseSequence();
		
		
		User user = new User();
		dataSequence.set_id(User.SEQUENCE_NAME);
		dataSequence.setSequence("1");
		user.set_id("1");
		user.setFirstName("Test");
		user.setLastName("User");
		user.setCreatedBy("system");
		user.setCreatedOn(new Date());
		user.setUpdatedBy("system");
		user.setUpdatedOn(new Date());
		user.setActiveInd(Boolean.TRUE);
		user.setAddress("Unknown");
		user.setPincode(000000);
		user.setEmailId("abc.test@test.com");
		user.setMiddleName("");
		user.setContactNumber("+91-7438783478");
		user.setUserType("Retailer");
		dbSeqRepo.save(dataSequence);
		userRepo.save(user);
		
		UserType userType = new UserType();
		dataSequence.set_id(UserType.SEQUENCE_NAME);
		dataSequence.setSequence("1");
		userType.set_id("1");
		userType.setUserTypeName("Retailer");
		userType.setCreatedBy("system");
		userType.setUpdatedBy("system");
		userType.setCreatedOn(new Date());
		userType.setUpdatedOn(new Date());
		dbSeqRepo.save(dataSequence);
		userTypeRepo.save(userType);
		dataSequence.setSequence("2");
		userType.set_id("2");
		userType.setUserTypeName("Consumer");
		dbSeqRepo.save(dataSequence);
		userTypeRepo.save(userType);
		
	}
}
