package com.hungeraid.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hungeraid.model.Invoices;
import com.hungeraid.services.InvoiceService;

@RestController
@RequestMapping(path = "/api/invoice")
public class InvoiceController {

	@Autowired
	private InvoiceService invoiceService;

	@RequestMapping(path = "/all", method = RequestMethod.GET)
	public List<Invoices> getAllInvoices() {
		return invoiceService.getAllInvoices();
	}
}
