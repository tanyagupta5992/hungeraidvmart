package com.hungeraid.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hungeraid.model.SoldItems;
import com.hungeraid.services.SoldItemsService;

@RestController
@RequestMapping(path = "/api/soldItem")
public class SoldItemController {

	@Autowired
	private SoldItemsService soldItemsService;

	@RequestMapping(path = "/all", method = RequestMethod.GET)
	public List<SoldItems> getAllSoldItems() {
		return soldItemsService.getAllSoldItems();
	}
}
