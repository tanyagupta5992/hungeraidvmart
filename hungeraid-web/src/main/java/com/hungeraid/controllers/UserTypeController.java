package com.hungeraid.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hungeraid.model.UserType;
import com.hungeraid.services.UserTypeService;

@RestController
@RequestMapping(path = "/api/userType")
public class UserTypeController {

	@Autowired
	private UserTypeService userTypeService;

	@RequestMapping(path = "/all", method = RequestMethod.GET)
	public List<UserType> getAllUserTypes() {
		return userTypeService.getAllUserType();
	}

}
