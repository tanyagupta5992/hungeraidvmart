package com.hungeraid.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hungeraid.model.Cart;
import com.hungeraid.services.CartService;

@RestController
@RequestMapping(path = "/api/cart")
public class CartController {

	@Autowired
	private CartService cartService;

	@RequestMapping(path = "/all", method = RequestMethod.GET)
	public List<Cart> getAllCarts() {
		return cartService.getAllCarts();
	}
}
