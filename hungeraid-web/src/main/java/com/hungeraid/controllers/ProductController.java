package com.hungeraid.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hungeraid.model.Products;
import com.hungeraid.services.ProductsService;

@RestController
@RequestMapping(path = "/api/product")
public class ProductController {

	@Autowired
	private ProductsService productsService;

	@RequestMapping(path = "/all", method = RequestMethod.GET)
	public List<Products> getAllProducts() {
		return productsService.getAllProducts();
	}

}
