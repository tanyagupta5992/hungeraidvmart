package com.hungeraid.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hungeraid.model.User;
import com.hungeraid.services.UserService;

@RestController
@RequestMapping(path = "/api/users")
public class UsersController {

	@Autowired
	private UserService userService;

	@RequestMapping(path = "/all", method = RequestMethod.GET)
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}

}
