package com.hungeraid.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hungeraid.model.ProductCategory;
import com.hungeraid.services.ProductCategoryService;

@RestController
@RequestMapping(path = "/api/productCategory")
public class ProductCategoryController {

	@Autowired
	private ProductCategoryService productCategoryService;

	@RequestMapping(path = "/all", method = RequestMethod.GET)
	public List<ProductCategory> getAllProductCategories() {
		return productCategoryService.getAllProductCategory();
	}
}
